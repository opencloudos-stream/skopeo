%bcond_with check
%global import_path github.com/containers/%{name}

Summary:       Inspect container images and repositories on registries
Name:          skopeo
Version:       1.17.0
Release:       1%{?dist}
License:       ASL 2.0
URL:           https://%{import_path}
Source0:       %{url}/archive/refs/tags/v%{version}.tar.gz #/%{name}-%{version}.tar.gz

BuildRequires: make git glib2-devel
BuildRequires: go-md2man golang >= 1.16.6 go-rpm-macros
BuildRequires: gpgme-devel libassuan-devel
BuildRequires: pkgconfig(devmapper)
Requires:      containers-common
Requires:      system-release

%description
skopeo is a command line utility that performs various operations on container images
and image repositories.
skopeo does not require the user to be running as root to do most of its operations.
skopeo does not require a daemon to be running to perform its operations.


%package tests
Summary: Tests for %{name}
Requires: %{name} = %{version}-%{release}
Requires: gnupg openssl
Requires: jq httpd-tools
%if %{with podman}
Requires: podman
%endif

%description tests
This package contains system tests for %{name}.


%prep
%autosetup -Sgit %{name}-%{version}
sed -i 's/install-binary: bin\/%{name}/install-binary:/' Makefile
sed -i 's/completions: bin\/%{name}/completions:/' Makefile
sed -i 's/install-docs: docs/install-docs:/' Makefile


%build
mkdir -p src/github.com/containers
ln -s ../../../ src/%{import_path}

mkdir -p vendor/src
for v in vendor/*; do
    if test ${v} = vendor/src; then continue; fi
    if test -d ${v}; then
      mv ${v} vendor/src/
    fi
done

export GOPATH=$(pwd):$(pwd)/vendor
export GO111MODULE=off
export CGO_CFLAGS="%{optflags} -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64"
export BUILDTAGS="exclude_graphdriver_btrfs btrfs_noversion $(hack/libdm_tag.sh)"
mkdir -p bin
%gobuild -o bin/%{name} ./cmd/%{name}
%{__make} docs


%install
make install-binary install-docs install-completions DESTDIR=%{buildroot} PREFIX=%{_prefix}

install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav systemtest/* %{buildroot}/%{_datadir}/%{name}/test/system/


%check
# ProxySuite.TestProxy failed, because registry is needed
%if %{with check}
export GOPATH=$(pwd)/vendor:$(pwd)
export PATH=${PATH}:%{buildroot}/usr/bin
%gotest %{import_path}/integration
%endif


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}.fish
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}

%files tests
%license LICENSE
%{_datadir}/%{name}/test


%changelog
* Wed Dec 04 2024 jackeyji <jackeyji@tencent.com> - 1.17.0-1
- [Type] security
- [DESC] upgrade to 1.17.0 to fix CVE-2024-9676

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.14.4-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.14.4-2
- Rebuilt for loongarch release

* Wed Jun 05 2024 jackeyji <jackeyji@tencent.com> - 1.14.4-1
- [Type] security
- [DESC] upgrade to 1.14.4 to fix CVE-2024-3727 CVE-2024-28180 and CVE-2024-24786

* Wed Mar 06 2024 jackeyji <jackeyji@tencent.com> - 1.14.2-1
- Upgrade to 1.14.2 to adapt moby

* Fri Dec 29 2023 jackeyji <jackeyji@tencent.com> - 1.13.0-3
- Rebuilt for golang CVE-2023-39318 and CVE-2023-39319

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.13.0-2
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Jul 13 2023 rockerzhu <rockerzhu@tencent.com> - 1.13.0-1
- Upgrade to 1.13.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.11.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.11.1-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Mar 30 2023 rockerzhu <rockerzhu@tencent.com> - 1.11.1-1
- Initial build
